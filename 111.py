from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.clock import Clock
from threading import Thread
import ccxt

class CryptoApp(App):
    def build(self):
        self.symbol_input = TextInput(hint_text='Введите команду', multiline=False)
        self.result_label = Label(text='', size_hint_y=None, height=44)

        layout = BoxLayout(orientation='vertical', padding=10, spacing=10)
        layout.add_widget(self.symbol_input)
        layout.add_widget(Button(text='Выполнить', on_press=self.on_button_click))
        layout.add_widget(self.result_label)

        # Add buttons for specific cryptocurrencies
        crypto_buttons_layout = BoxLayout(spacing=10)
        cryptocurrencies = ['BTC', 'ETH', 'XRP', 'LTC']
        for crypto in cryptocurrencies:
            button = Button(text=crypto, on_press=lambda instance, c=crypto: self.get_crypto_price_and_display(c))
            crypto_buttons_layout.add_widget(button)

        layout.add_widget(crypto_buttons_layout)

        # Schedule updating cryptocurrency prices every 30 seconds
        Clock.schedule_interval(self.update_crypto_prices, 30)

        return layout

    def display_help(self):
        help_text = (
            'Привет! Я программа для отображения курса криптовалют.\n\n'
            'Используй команду /crypto "символ", чтобы получить курс.\n\n'
            'Поддерживаемые символы криптовалют:\n'
            'btcusdt - Bitcoin в USDT\n'
            'ethusdt - Ethereum в USDT\n'
            'xrpusdt - Ripple в USDT\n'
            'ltcusdt - Litecoin в USDT\n'
            'Другие токены тоже поддерживаются\n'
            'Product by MinVin (Igor Minenkov)'
        )
        self.show_popup("Справка", help_text)

    def get_crypto_price(self, symbol):
        exchange = ccxt.binance()
        ticker = exchange.fetch_ticker(symbol + 'USDT')
        return ticker['last']

    def get_crypto_price_and_display(self, symbol):
        Thread(target=self.fetch_and_display_price, args=(symbol,), daemon=True).start()

    def fetch_and_display_price(self, symbol):
        try:
            price = self.get_crypto_price(symbol)
            self.result_label.text = f'Текущий курс {symbol.upper()}: ${price}'
        except Exception as e:
            self.show_popup("Ошибка", f'Произошла ошибка: {e}')

    def on_button_click(self, instance):
        user_input = self.symbol_input.text.strip()

        if user_input.lower() in ('/start', '/help'):
            self.display_help()
        elif user_input.startswith('/crypto '):
            crypto_symbol = user_input.split()[1].upper()
            self.get_crypto_price_and_display(crypto_symbol)
        else:
            self.show_popup("Ошибка", 'Некорректная команда. Введите /help для отображения справки.')

    def show_popup(self, title, content):
        popup = Popup(title=title, content=Label(text=content), size_hint=(None, None), size=(400, 200))
        popup.open()

    def update_crypto_prices(self, dt):
        # Update cryptocurrency prices for the buttons every 30 seconds
        cryptocurrencies = ['BTC', 'ETH', 'XRP', 'LTC']
        for crypto in cryptocurrencies:
            Thread(target=self.fetch_and_update_button, args=(crypto,), daemon=True).start()

    def fetch_and_update_button(self, crypto):
        try:
            price = self.get_crypto_price(crypto)
            # Find the button and update its text
            for widget in self.root.children:
                if isinstance(widget, BoxLayout) and widget.children and isinstance(widget.children[0], Button) \
                        and widget.children[0].text == crypto:
                    widget.children[0].text = f'{crypto} ${price:.2f}'
        except Exception as e:
            print(f'Error updating {crypto} price: {e}')

if __name__ == '__main__':
    CryptoApp().run()
